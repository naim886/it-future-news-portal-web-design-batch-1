$(document).ready(function (){
    $('#more_menu').on('click', function () {
        $('#mega_menu').slideToggle(500)
        $('#menu').slideToggle(500)
    })
    $('#search_toggle').on('click', function () {
        $('#menu_container').slideToggle()
        $('#search_area').slideToggle()
    })
    $('#search_close').on('click', function () {
        $('#menu_container').slideToggle()
        $('#search_area').slideToggle()
    })
})